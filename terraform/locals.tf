locals {
  folder_id = "b1gkguhma68ug1mggu9c"
  service-accounts = toset([
    "bingo-sa-super",
    "bingo-ig-sa-super",
  ])
  bingo-sa-roles = toset([
    "container-registry.images.puller",
    "monitoring.editor",
  ])
  bingo-ig-sa-roles = toset([
    "compute.editor",
    "iam.serviceAccounts.user",
    "load-balancer.admin",
    "vpc.publicAdmin",
    "vpc.user",
  ])
}