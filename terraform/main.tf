terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  zone                     = "ru-central1-a"
  service_account_key_file = "./tf_key.json"
  cloud_id                 = "b1g0vh6uspd0m39d5er6"
  folder_id                = local.folder_id
}


data "yandex_compute_image" "coi" {
  family = "container-optimized-image"
}

resource "yandex_vpc_network" "default" {
}

resource "yandex_vpc_subnet" "subnet-1" {
  zone           = "ru-central1-a"
  network_id     = resource.yandex_vpc_network.default.id
  v4_cidr_blocks = ["192.168.10.0/24"]
  route_table_id = yandex_vpc_route_table.rt.id
}

resource "yandex_vpc_gateway" "nat_gateway" {
  name = "internet"
  shared_egress_gateway {}
}

resource "yandex_vpc_route_table" "rt" {
  name       = "test-route-table"
  network_id = yandex_vpc_network.default.id

  static_route {
    destination_prefix = "0.0.0.0/0"
    gateway_id         = yandex_vpc_gateway.nat_gateway.id
  }
}

resource "yandex_container_registry" "registry1" {
  name = "registry1"
}

resource "yandex_iam_service_account" "service-accounts" {
  for_each = local.service-accounts
  name     = each.key
}
resource "yandex_resourcemanager_folder_iam_member" "bingo-roles" {
  for_each  = local.bingo-sa-roles
  folder_id = local.folder_id
  member    = "serviceAccount:${yandex_iam_service_account.service-accounts["bingo-sa-super"].id}"
  role      = each.key
}

resource "yandex_resourcemanager_folder_iam_member" "bingo-ig-roles" {
  for_each  = local.bingo-ig-sa-roles
  folder_id = local.folder_id
  member    = "serviceAccount:${yandex_iam_service_account.service-accounts["bingo-ig-sa-super"].id}"
  role      = each.key
}

resource "yandex_compute_instance_group" "bingo" {
  name               = "bingo"
  service_account_id = yandex_iam_service_account.service-accounts["bingo-ig-sa-super"].id
  instance_template {
    service_account_id = yandex_iam_service_account.service-accounts["bingo-sa-super"].id
    resources {
      core_fraction = 5
      memory        = 1
      cores         = 2
    }
    boot_disk {
      mode = "READ_WRITE"
      initialize_params {
        type     = "network-hdd"
        size     = "30"
        image_id = data.yandex_compute_image.coi.id
      }
    }
    network_interface {
      network_id = yandex_vpc_network.default.id
      subnet_ids = ["${yandex_vpc_subnet.subnet-1.id}"]
    }
    metadata = {
      docker-compose = templatefile(
        "${path.module}/docker-compose.yaml",
        {
          folder_id   = "${local.folder_id}",
          registry_id = "${yandex_container_registry.registry1.id}",
        }
      )
      user-data = templatefile(
        "${path.module}/cloud_config.yaml",
        {
          folder_id   = "${local.folder_id}",
        }
      )
    }
    scheduling_policy {
      preemptible = true
    }
  }
  scale_policy {
    fixed_scale {
      size = 2
    }
  }

  deploy_policy {
    max_unavailable = 2
    max_creating    = 2
    max_expansion   = 2
    max_deleting    = 2
  }

  allocation_policy {
    zones = ["ru-central1-a"]
  }

  load_balancer {
    target_group_name        = "target-group"
    target_group_description = "load balancer target group"
  }
}

resource "yandex_lb_network_load_balancer" "lb-1" {
  name = "network-load-balancer-1"

  listener {
    name = "network-load-balancer-1-listener"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_compute_instance_group.bingo.load_balancer.0.target_group_id

    healthcheck {
      name = "http"
      http_options {
        port = 29964
        path = "/ping"
      }
    }
  }
}